package es.bsc.inb.elixir.openebench.git.populous;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonPointer;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonStructure;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.PropertyOrderStrategy;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;
import javax.json.stream.JsonParser;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.util.SystemReader;

/**
 * @author Dmitry Repchevsky
 */

public class Main {
    
    private final static String HELP = 
            "openebench-populous -g -u -p [-b]\n\n" +
            "parameters:\n\n" +
            "-h (--help)     - this help message\n" +
            "-g (--git)      - git endpoint\n" +
            "-u (--user)     - github username\n" +
            "-p (--password) - github pasword\n" +
            "-b (--branch)   - git remote branch ['origin/master']\n\n" +
            "example: >java -jar openebench-populous.jar -g https://myrepo.git -u redmitry -p xyz\n";

    public final static String METRICS_ENDPOINT;

    public final static String BRANCH = "origin/master";
    
    private final static List<JsonPointer> DISREGARDED_PROPERTIES = new ArrayList<>();

    static {
        String metrics_endpoint = null;
        
        try (InputStream in = Main.class.getClassLoader().getResourceAsStream("META-INF/config.properties")) {
            if (in != null) {
                final Properties properties = new Properties();
                properties.load(in);

                metrics_endpoint = properties.getProperty("METRICS_ENDPOINT");
                
                final String disregarded = properties.getProperty("DISREGARDED_PROPERTIES");
                if (disregarded != null) {
                    for (String jpointer : disregarded.split(" ")) {
                        DISREGARDED_PROPERTIES.add(Json.createPointer(jpointer));
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        METRICS_ENDPOINT = metrics_endpoint;
    }
    
    public static void main(String[] args) throws IOException {
        
        Map<String, List<String>> params = parameters(args);
        
        if (params.get("-h") != null ||
            params.get("--help") != null) {
            System.out.println(HELP);
            System.exit(0);
        }
        
        List<String> git = params.get("-g");
        if (git == null) {
            git = params.get("--git");
        }

        List<String> user = params.get("-u");
        if (user == null) {
            user = params.get("--user");
        }

        List<String> password = params.get("-p");
        if (password == null) {
            password = params.get("--password");
        }

        List<String> branch = params.get("-b");
        if (branch == null) {
            branch = params.get("--branch");
        }
        final String b = branch == null || branch.isEmpty() ? null : branch.get(0);

        final String g = git == null || git.isEmpty() ? null : git.get(0);
        final String u = user == null || user.isEmpty() ? null : user.get(0);
        final String p = password == null || password.isEmpty() ? null : password.get(0);

        if (u == null || u.isEmpty() || p == null || p.isEmpty()) {
            System.out.println(HELP);
        } else {
            process(g, b != null ? b : BRANCH, u, p);
        }
    }
    
    private static void process(final String endpoint, final String branch, 
            final String user, final String password) {

        final int idx = branch.lastIndexOf('/');
        final String local = idx < 0 ? branch : branch.substring(idx + 1);

        // ignore file names validation as we do not use 'real' file system
        SystemReader old = SystemReader.getInstance();
        SystemReader.setInstance(new JgitSytemReaderHack(old));

        Configuration config = Configuration.unix().toBuilder()
                                    .setWorkingDirectory("/")
                                    .build();

        FileSystem fs = Jimfs.newFileSystem(config);
        final Path root = fs.getPath("/");
        
        try (Git git = Git.cloneRepository()
                         .setURI(endpoint)
                         .setDirectory(root)
                         .setBranch(local)
                         .setRemote(branch)
                         .call()) {
            
            setMetrics(git);
            
            git.add().addFilepattern(".").call();
            git.commit().setMessage("openebench metrics " + Instant.now()).call();
            git.push().setRemote(BRANCH).setCredentialsProvider(
                    new UsernamePasswordCredentialsProvider(user, password)).call();
        } 
        catch (GitAPIException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }
    }

    private static void setMetrics(final Git git) {
        
        HashMap<String, Path> files = new HashMap();

        // index .json files by biotoolsID
        try (Stream<Path> stream = Files.walk(git.getRepository().getWorkTreePath())){
            final Iterator<Path> iter = stream.iterator();
            while (iter.hasNext()) {
                final Path path = iter.next();
                if (Files.isRegularFile(path) && path.toString().endsWith("oeb.json")) {
                    final String biotoolsID = getBiotoolsID(path);
                    if (biotoolsID != null) {
                        files.put(biotoolsID, path);
                    }
                }
            }
        } catch(IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        final Jsonb jsonb = JsonbBuilder.create(
                    new JsonbConfig().withPropertyOrderStrategy(PropertyOrderStrategy.LEXICOGRAPHICAL)
                                     .withFormatting(true));
            
        // iterate over all 'biotools:' tools
        try (BufferedInputStream in = new BufferedInputStream(new URL(METRICS_ENDPOINT).openStream());
             JsonParser parser = Json.createParser(in)) {
            
            if (parser.hasNext() &&
                parser.next() == JsonParser.Event.START_ARRAY) {

                try (Stream<JsonValue> stream = parser.getArrayStream()) {
                    final Iterator<JsonValue> iter = stream.iterator();
                    while(iter.hasNext()) {
                        final JsonValue value = iter.next();

                        if (value.getValueType() == ValueType.OBJECT) {
                            final JsonObject jmetrics = value.asJsonObject();

                            final String _id = jmetrics.getString("@id", null);
                            if (_id != null) {
                                String id = _id.substring(METRICS_ENDPOINT.length() + 1);
                                final int idx = id.indexOf('/');
                                if (idx > 0) {
                                    id = id.substring(0, idx);
                                }
                                
                                final String[] tokens = id.split(":");
                                id = tokens[tokens.length == 1 ? 0 : 1];
                                
                                final GitMetrics metrics = jsonb.fromJson(jmetrics.toString(), GitMetrics.class);
                                metrics.setId(_id);
                                metrics.setBiotoolsId(id);
                                writeFile(files.get(id), jsonb, metrics);
                            }
                        }
                    }
                }
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void writeFile(final Path file, final Jsonb jsonb, final GitMetrics metrics) {
        if (file != null) {
            final String tool_file_path = file.toString();
            final Path metrics_file_path = file.getRoot().resolve(file.toString().substring(0, tool_file_path.length() - "json".length()) + "metrics.json");

            try {
                if (Files.exists(metrics_file_path)) {
                    JsonObject object;
                    final String json = jsonb.toJson(metrics);
                    try (JsonReader reader = Json.createReader(new StringReader(json))) {
                        object = reader.readObject();
                        for (JsonPointer jpointer : DISREGARDED_PROPERTIES) {
                            try {
                                object = jpointer.remove(object);
                            } catch (JsonException ex) {
                                // no pointer...
                            }
                        }
                    }

                    final JsonStructure structure;
                    try (JsonReader reader = Json.createReader(Files.newBufferedReader(metrics_file_path))) {
                        structure = reader.read();
                    }

                    final JsonGeneratorFactory factory = Json.createGeneratorFactory(
                            Collections.singletonMap(JsonGenerator.PRETTY_PRINTING, true));

                    try (JsonGenerator writer = factory.createGenerator(Files.newBufferedWriter(metrics_file_path))) {
                        writer.writeStartArray();
                        
                        // write 'common' tool metrics first
                        if (metrics.getId() != null && metrics.getId().contains("/")) {
                            writer.write(object);
                            object = null;
                        }
                        
                        if (structure.getValueType() == ValueType.ARRAY) {
                            final JsonArray array = structure.asJsonArray();
                            for (int i = 0, n = array.size(); i < n; i++) {
                                final JsonValue value = array.get(i);
                                if (value.getValueType() == ValueType.OBJECT) {
                                    final String id = value.asJsonObject().getString("@id", null);
                                    if (id != null) {
                                        if (!id.equals(metrics.getId())) {
                                            writer.write(value);
                                        } else if (object != null) {
                                            writer.write(object);
                                            object = null;                                            
                                        }
                                    }
                                }
                            }
                        }
                        if (object != null) {
                            writer.write(object);
                        }
                        writer.writeEnd();
                    }
                } else {
                    try (Writer writer = Files.newBufferedWriter(metrics_file_path)) {
                        jsonb.toJson(Arrays.asList(metrics), writer);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
    
    private static String getBiotoolsID(final Path path) {
        try (JsonReader parser = Json.createReader(Files.newBufferedReader(path))) {
            final JsonStructure structure = parser.read();
            if (structure.getValueType() == ValueType.OBJECT) {
//                final JsonValue biotoolsID = structure.asJsonObject().get("biotoolsID");
//                if (biotoolsID != null && 
//                    biotoolsID.getValueType() == ValueType.STRING) {
//
//                    return ((JsonString)biotoolsID).getString().toLowerCase().trim();
//                } 
                final JsonValue value = structure.asJsonObject().get("summary");
                if (value != null && value.getValueType() == ValueType.OBJECT) {
                    final JsonValue biotoolsID = value.asJsonObject().get("biotoolsID");
                    if (biotoolsID != null && 
                        biotoolsID.getValueType() == ValueType.STRING) {

                        return ((JsonString)biotoolsID).getString();
                    }
                }
            }
        } catch(IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private static Map<String, List<String>> parameters(String[] args) {
        TreeMap<String, List<String>> parameters = new TreeMap();        
        List<String> values = null;
        for (String arg : args) {
            switch(arg) {
                case "-g":
                case "--git":
                case "-b":
                case "--branch":
                case "-u":
                case "--user":
                case "-p":
                case "--password":
                case "-h":
                case "--help": values = parameters.get(arg);
                               if (values == null) {
                                   values = new ArrayList(); 
                                   parameters.put(arg, values);
                               }
                               break;
                default: if (values != null) {
                    values.add(arg);
                }
            }
        }
        return parameters;
    }

}
