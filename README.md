### OpenEBench Populous is a utility to push OpenEBench metrics data into git repository.

The utility is based on modified version of [JGit](https://github.com/redmitry/jgit) libarary based on [NIO2](https://jcp.org/en/jsr/detail?id=203) API and 
[Jimfs](https://github.com/google/jimfs) in-memory file system. It allows wirking with git repositories directly, without writing anything on a disk.

The metrics Json files follow the OpenEBench tools monitoring metrics model
[metrics.json](https://gitlab.bsc.es/inb/elixir/tools-platform/openebench-metrics-model/blob/master/schemas/metrics.json).

Compiling:
```shell
>git clone https://gitlab.bsc.es/inb/elixir/tools-platform/openebench-git-populous.git
>cd openebench-git-populous
>mvn install
```

It will create **openebench-git-populous.jar** file in the **openebench-git-populous/target/** directory.

Running:
```shell
>java -jar openebench-git-populous.jar -g https://gitlab.bsc.es/xyz -u user -p password
```

where:

* -h         - help
* --git      - git repository
* --branch   - remote branch to push (default 'origin/master')
* --path     - remote path to put files
* --user     - git user name
* --password - git user password

The configuration of OpenEBench server REST metrics endpoint is hardcoded within the application:  
openebench-gir-populous.jar/META-INF/config.properties  

```
METRICS_ENDPOINT = https://openebench.bsc.es/monitor/metrics
```

Particular metrics may be excluded from being pushed to the git server providing a list of their Json Pointers:
```
DISREGARDED_PROPERTIES = /@timestamp
```
